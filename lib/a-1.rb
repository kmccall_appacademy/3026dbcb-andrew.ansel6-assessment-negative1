# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  currentnum = nums[0]
  newarr = []
  while currentnum < nums[-1]
    newarr << currentnum unless nums.include?(currentnum)
    currentnum += 1
  end
  newarr
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  arr = binary.split("")
  num_base10 = 0
  arr.each_with_index do |ele,idx|
    num_base10 += ele.to_i * 2 ** (arr.length - 1 - idx)
  end
  num_base10
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hash = {}
      self.each do |key,value|
        hash[key] = value if (yield key,value) == true
      end
      hash
  end

end #End of class

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    newhash = hash
    self.each do |key,value|
      newhash[key] = yield key,value, hash[key] if block_given? && hash[key] != nil 
      newhash[key] = self[key] if !block_given? && !newhash.key?(key)
    end
    hash
  end #End of method

end #End of class

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  zero = 2
  first = 1
  negfirst = -1

  if n == 0
    return zero
  elsif n == 1
    return first
  elsif n > 1
    two_previous = zero
    previous = first
    current = previous + two_previous
    i = 3
    while i <= n
      two_previous = previous
      previous = current
      current = previous + two_previous
      i += 1
    end
  else
    two_previous = zero
    previous = negfirst
    current = two_previous - previous
    i = -3
    while i >= n
      two_previous = previous
      previous = current
      current = two_previous - previous
      i -= 1
    end
  end
  current
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  chars = string.split("")
  longest = ""

  chars.each_with_index do |char, idx|
    counter = idx + 2
    while counter < chars.length
      if chars[idx..counter] == chars[idx..counter].reverse
        longest = chars[idx..counter] if chars[idx..counter].length > longest.length
      end
      counter += 1
    end

  end

  return false if longest.length <= 2
  longest.length
end
